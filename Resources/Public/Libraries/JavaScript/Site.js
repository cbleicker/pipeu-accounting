jQuery(document).on('ready', function () {

	documentDataTable = jQuery('.dataTable').DataTable({
		stateSave: true,
		iDisplayLength: 25,
		order: [
			[ 5, 'desc' ]
		],
		"aoColumns": [
			{'bSearchable': false},
			{'bSearchable': false},
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null
		]
	});

	jQuery(document).on('click', 'a.api-archived-set', function (event) {
		event.preventDefault();
		var action = jQuery(this).attr('href');
		var method = 'GET';
		var context = jQuery(this);
		jQuery.ajax({
			type: method,
			url: action,
			context: context,
			success: function (data) {
				console.debug(data);
				jQuery.each(data.document, function (i, document) {
					documentDataTable.row('#doc_' + document.identity).remove().draw(false);
				});
				documentDataTable.draw(false);
			}
		});
	});

	jQuery(document).on('click', 'a.api-state-set', function (event) {
		event.preventDefault();
		var action = jQuery(this).attr('href');
		var method = 'GET';
		var context = jQuery(this);
		jQuery.ajax({
			type: method,
			url: action,
			context: context,
			success: function (data) {
				if (data.document.primaryState.code == 1401712161) {
					jQuery(this).parent().find('.btn-paymentReminder').remove();
					console.debug(jQuery(this).parent());
				}
				var state = '<a href="/accounting/api/document/removestate?document[__identity]=' + data.document.identity + '&state[__identity]=' + data.document.primaryState.__identity + '"' + 'class="api-state-remove badge badge-' + data.document.primaryState.severity + ' badge-primary"><i class="glyphicon glyphicon-info-sign"></i> ' + data.document.primaryState.message + '</a>';
				jQuery('#doc_' + data.document.identity + ' .states .badge-primary').removeClass('badge-primary');
				jQuery('#doc_' + data.document.identity + ' .states').prepend(state);
			}
		});
	});

	jQuery(document).on('click', 'a.api-state-remove', function (event) {
		event.preventDefault();
		var action = jQuery(this).attr('href');
		var method = 'GET';
		var context = jQuery(this);
		jQuery.ajax({
			type: method,
			url: action,
			context: context,
			success: function (data) {
				jQuery(this).remove();
				jQuery('#doc_' + data.document.identity + ' .states .badge').first().addClass('badge-primary');
			}
		});
	});

	jQuery(document).on('click', 'a.api-convert', function (event) {
		event.preventDefault();
		var action = jQuery(this).attr('href');
		var method = 'GET';
		var context = jQuery(this);
		jQuery.ajax({
			type: method,
			url: action,
			context: context,
			success: function (data) {
				location.reload();
			}
		});
	});

});