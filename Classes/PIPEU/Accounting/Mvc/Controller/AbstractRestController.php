<?php
/*                                                                        		*
 * This script belongs to the TYPO3 Flow package "BLEICKER.ArtManager.Rest".    *
 *                                                                        		*
 *                                                                        		*/

namespace PIPEU\Accounting\Mvc\Controller;

use TYPO3\Flow\Annotations as Flow;
use PIPEU\Accounting\Mvc\View\JsonView;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Mvc\View\ViewInterface;

/**
 * Class AbstractRestController
 *
 * @package PIPEU\Accounting\Mvc\Controller
 */
abstract class AbstractRestController extends ActionController {

	/**
	 * @var \TYPO3\Flow\Security\Context
	 * @Flow\Inject
	 */
	protected $securityContext;

	/**
	 * The default view object to use if none of the resolved views can render
	 * a response for the current request.
	 *
	 * @var string
	 */
	protected $defaultViewObjectName = 'PIPEU\Accounting\Mvc\View\JsonView';

	/**
	 * @var JsonView
	 */
	protected $view;

	/**
	 * Name of the action method argument which acts as the resource for the
	 * RESTful controller. If an argument with the specified name is passed
	 * to the controller, the show, update and delete actions can be triggered
	 * automatically.
	 *
	 * @var string
	 */
	protected $resourceArgumentName = 'resource';

	/**
	 * @var string
	 */
	protected $errorsArgumentName = 'errors';

	/**
	 * @var \TYPO3\Flow\Exception
	 */
	protected $storedExceptionForErrorAction;

	/**
	 * @var array
	 */
	protected $viewConfigurations = array(
		'DEFAULT' => array(),
		'LIST'    => array()
	);

	/**
	 * @param ViewInterface $view
	 * @return void
	 */
	protected function initializeView(ViewInterface $view) {
		parent::initializeView($view);
		if($view instanceof JsonView){
			$view->setVariablesToRender(array($this->resourceArgumentName));
			$view->setOption('jsonEncodingOptions', JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
			$configuration = array($this->resourceArgumentName => $this->viewConfigurations['DEFAULT']);
			$view->setConfiguration($configuration);
		}
		$actionInitializationViewMethodName = 'initializeView' . ucfirst($this->actionMethodName);
		if (method_exists($this, $actionInitializationViewMethodName)) {
			call_user_func(array($this, $actionInitializationViewMethodName));
		}
	}

	/**
	 * @return void
	 */
	protected function initializeViewListAction() {
		$configuration = array(
			$this->resourceArgumentName => $this->viewConfigurations['LIST']
		);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * Allow creation of resources in createAction()
	 *
	 * @return void
	 */
	protected function initializeCreateAction() {
		$propertyMappingConfiguration = $this->arguments->getArgument($this->resourceArgumentName)->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', \TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
		$propertyMappingConfiguration->allowAllProperties();
	}

	/**
	 * Allow modification of resources in updateAction()
	 *
	 * @return void
	 */
	protected function initializeUpdateAction() {
		$propertyMappingConfiguration = $this->arguments->getArgument($this->resourceArgumentName)->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', \TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, TRUE);
		$propertyMappingConfiguration->allowAllProperties();
	}

	/**
	 * Redirects the web request to another uri.
	 * NOTE: This method only supports web requests and will throw an exception
	 * if used with other request types.
	 *
	 * @param mixed $uri Either a string representation of a URI or a \TYPO3\Flow\Http\Uri object
	 * @param integer $delay (optional) The delay in seconds. Default is no delay.
	 * @param integer $statusCode (optional) The HTTP status code for the redirect. Default is "303 See Other"
	 * @return void
	 * @throws \TYPO3\Flow\Mvc\Exception\StopActionException
	 */
	protected function redirectToUri($uri, $delay = 0, $statusCode = 303) {
		try {
			parent::redirectToUri($uri, $delay, $statusCode);
		} catch (\TYPO3\Flow\Mvc\Exception\StopActionException $exception) {
			if ($this->request->getFormat() === 'json') {
				$this->response->setContent('');
			}
			throw $exception;
		}
	}

	/**
	 * @return void
	 * @todo check storedExceptionForErrorAction is realy needed
	 */
	protected function errorAction() {
		$this->view->setVariablesToRender(array($this->errorsArgumentName));
		$this->response->setStatus(500, 'Bad Request. See errors section for details');
		if ($this->storedExceptionForErrorAction instanceof \TYPO3\Flow\Exception) {
			$this->view->assign($this->errorsArgumentName, new \TYPO3\Flow\Error\Error($this->storedExceptionForErrorAction->getMessage(), $this->storedExceptionForErrorAction->getCode()));
		}
		if ($this->arguments->getValidationResults()->hasErrors()) {
			$this->view->assign($this->errorsArgumentName, $this->arguments->getValidationResults()->getFlattenedErrors());
		}
	}

	/**
	 * Prepares a view for the current action and stores it in $this->view.
	 * By default, this method tries to locate a view with a name matching
	 * the current action.
	 *
	 * @return \TYPO3\Flow\Mvc\View\ViewInterface the resolved view
	 * @throws \TYPO3\Flow\Mvc\Exception\ViewNotFoundException if no view can be resolved
	 */
	protected function resolveView() {
		$viewsConfiguration = $this->viewConfigurationManager->getViewConfiguration($this->request);

		if ($this->defaultViewObjectName !== NULL) {
			$viewObjectName = $this->defaultViewObjectName;
		} else {
			$viewObjectName = 'TYPO3\Flow\Mvc\View\JsonView';
		}

		if (isset($viewsConfiguration['options'])) {
			$view = $this->objectManager->get($viewObjectName, $viewsConfiguration['options']);
		} else {
			$view = $this->objectManager->get($viewObjectName);
		}

		if (!isset($view)) {
			throw new \TYPO3\Flow\Mvc\Exception\ViewNotFoundException(sprintf('Could not resolve view for action "%s" in controller "%s"', $this->request->getControllerActionName(), get_class($this)), 1355153185);
		}
		if (!$view instanceof \TYPO3\Flow\Mvc\View\ViewInterface) {
			throw new \TYPO3\Flow\Mvc\Exception\ViewNotFoundException(sprintf('View has to be of type ViewInterface, got "%s" in action "%s" of controller "%s"', get_class($view), $this->request->getControllerActionName(), get_class($this)), 1355153188);
		}

		return $view;
	}

	/**
	 * Invokes only on writing request methods (POST/PUT/PATCH)
	 *
	 * @return void
	 */
	protected function initializeActionMethodValidators() {
		if ($this->request->getHttpRequest()->getMethod() === 'POST' || $this->request->getHttpRequest()->getMethod() === 'PUT' || $this->request->getHttpRequest()->getMethod() === 'PATCH') {
			parent::initializeActionMethodValidators();
		}
	}
}
