<?php
/*                                                                        		*
 * This script belongs to the TYPO3 Flow package "BLEICKER.ArtManager.Rest".    *
 *                                                                        		*
 *                                                                        		*/

namespace PIPEU\Accounting\Mvc\View;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Mvc\View\JsonView as JsonViewOrigin;
use TYPO3\Flow\Resource\Resource as FileResource;
use TYPO3\Flow\Http\Response;

/**
 * Class JsonView
 *
 * @package PIPEU\Accounting\Mvc\View
 */
class JsonView extends JsonViewOrigin {

	/**
	 * @var \TYPO3\Flow\Resource\Publishing\ResourcePublisher
	 * @Flow\Inject
	 */
	protected $resourcePublisher;

	/**
	 * Supported options
	 *
	 * @var array
	 */
	protected $supportedOptions = array(
		'jsonEncodingOptions' => array(0, 'Bitmask of supported Encoding options. See http://php.net/manual/en/json.constants.php', 'integer')
	);

	/**
	 * Transforms the value view variable to a serializable
	 * array represantion using a YAML view configuration and JSON encodes
	 * the result.
	 *
	 * @return string The JSON encoded variables
	 */
	public function render() {
		/** @var Response $response */
		$response = $this->controllerContext->getResponse();
		$response->setHeader('Content-Type', 'application/json');
		$propertiesToRender = $this->renderArray();
		$options            = $this->getOption('jsonEncodingOptions');
		return json_encode($propertiesToRender, $options);
	}

	/**
	 * Loads the configuration and transforms the value to a serializable
	 * array.
	 *
	 * @return array An array containing the values, ready to be JSON encoded
	 */
	protected function renderArray() {
		$valueToRender = array();
		foreach ($this->variablesToRender as $variableName) {
			$valueToRender[$variableName] = isset($this->variables[$variableName]) ? $this->variables[$variableName] : NULL;
		}
		$configuration = $this->configuration;

		return $this->transformValue($valueToRender, $configuration);
	}

	/**
	 * Transforms a value depending on type recursively using the
	 * supplied configuration.
	 *
	 * @param mixed $value The value to transform
	 * @param array $configuration Configuration for transforming the value
	 * @return array The transformed value
	 */
	protected function transformValue($value, array $configuration) {
		if (is_array($value) || $value instanceof \ArrayAccess) {
			$array = array();
			foreach ($value as $key => $element) {
				if (isset($configuration['_descendAll']) && is_array($configuration['_descendAll'])) {
					$array[$key] = $this->transformValue($element, $configuration['_descendAll']);
				} else {
					if (isset($configuration['_only']) && is_array($configuration['_only']) && !in_array($key, $configuration['_only'])) {
						continue;
					}
					if (isset($configuration['_exclude']) && is_array($configuration['_exclude']) && in_array($key, $configuration['_exclude'])) {
						continue;
					}
					$array[$key] = $this->transformValue($element, isset($configuration[$key]) ? $configuration[$key] : array());
				}
			}
			return $array;
		} elseif (is_object($value) && $value instanceof \JsonSerializable) {
			return $this->transformValue($value->jsonSerialize(), $configuration);
		} elseif (isset($configuration['_forceExposeObjectIdentifierOnly']) && $configuration['_forceExposeObjectIdentifierOnly'] === TRUE && ($this->reflectionService->isClassAnnotatedWith($this->reflectionService->getClassNameByObject($value), 'TYPO3\Flow\Annotations\Entity') || $this->reflectionService->isClassAnnotatedWith($this->reflectionService->getClassNameByObject($value), 'TYPO3\Flow\Annotations\ValueObject') || $this->reflectionService->isClassAnnotatedWith($this->reflectionService->getClassNameByObject($value), 'Doctrine\ORM\Mapping\Entity'))) {
			$objectIdentifier = $this->persistenceManager->getIdentifierByObject($value);
			return $objectIdentifier;
		} elseif (is_object($value)) {
			return $this->transformObject($value, $configuration);
		} else {
			return $value;
		}
	}

	/**
	 * Traverses the given object structure in order to transform it into an
	 * array structure.
	 *
	 * @param object $object Object to traverse
	 * @param array $configuration Configuration for transforming the given object or NULL
	 * @return array Object structure as an array
	 */
	protected function transformObject($object, array $configuration) {

		if ($object instanceof \DateTime) {
			return $object->format(\DateTime::W3C);
		}

		if ($object instanceof Message) {
			return (string)$object;
		}

		if($object instanceof FileResource){
			return $this->resourcePublisher->publishPersistentResource($object);
		}

		return parent::transformObject($object, $configuration);
	}
}
