<?php

namespace PIPEU\Accounting\ViewHelpers;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class OrdersInfoViewHelper
 *
 * @package PIPEU\Accounting\ViewHelpers
 */
class OrdersInfoViewHelper extends AbstractViewHelper {

	/**
	 * @param QueryResultInterface $orders
	 * @return string
	 */
	public function render(QueryResultInterface $orders) {

		$data['summary'] = 0;

		/** @var Invoice $document */
		while ($document = $orders->current()) {
			$data['summary'] += $document->getSummary()->getValue();
			$orders->next();
		}

		$this->templateVariableContainer->add('summary', new Money($data['summary']));
		$content = $this->renderChildren();
		$this->templateVariableContainer->remove('summary');

		return $content;
	}
}
