<?php

namespace PIPEU\Accounting\ViewHelpers;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class CreditsInfoViewHelper
 *
 * @package PIPEU\Accounting\ViewHelpers
 */
class CreditsInfoViewHelper extends AbstractViewHelper {

	/**
	 * @param QueryResultInterface $credits
	 * @return string
	 */
	public function render(QueryResultInterface $credits) {

		$data['summary'] = 0;

		/** @var Invoice $document */
		while ($document = $credits->current()) {
			$data['summary'] += $document->getSummary()->getValue();
			$credits->next();
		}

		$this->templateVariableContainer->add('summary', new Money($data['summary']));
		$content = $this->renderChildren();
		$this->templateVariableContainer->remove('summary');

		return $content;
	}
}
