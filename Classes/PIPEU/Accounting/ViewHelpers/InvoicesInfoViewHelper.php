<?php

namespace PIPEU\Accounting\ViewHelpers;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
/**
 * Class InvoicesInfoViewHelper
 *
 * @package PIPEU\Accounting\ViewHelpers
 */
class InvoicesInfoViewHelper extends AbstractViewHelper {

	/**
	 * @param QueryResultInterface $invoices
	 * @return string
	 */
	public function render(QueryResultInterface $invoices) {

		$data['summary'] = 0;
		$data['items'] = [];
		/** @var Invoice $document */
		while ($document = $invoices->current()) {
			$data['summary'] += $document->getSummary()->getValue();

			/** @var AbstractFacturaItem $facturaItem */
			while($facturaItem = $document->getFacturaItems()->current()){
				$document->getFacturaItems()->next();
			}

			$invoices->next();
		}

		$this->templateVariableContainer->add('summary', new Money($data['summary']));
		$content = $this->renderChildren();
		$this->templateVariableContainer->remove('summary');

		return $content;
	}
}
