<?php

namespace PIPEU\Accounting\ViewHelpers;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class ReversesInfoViewHelper
 *
 * @package PIPEU\Accounting\ViewHelpers
 */
class ReversesInfoViewHelper extends AbstractViewHelper {

	/**
	 * @param QueryResultInterface $reverses
	 * @return string
	 */
	public function render(QueryResultInterface $reverses) {

		$data['summary'] = 0;

		/** @var Invoice $document */
		while ($document = $reverses->current()) {
			$data['summary'] += $document->getSummary()->getValue();
			$reverses->next();
		}

		$this->templateVariableContainer->add('summary', new Money($data['summary']));
		$content = $this->renderChildren();
		$this->templateVariableContainer->remove('summary');

		return $content;
	}
}
