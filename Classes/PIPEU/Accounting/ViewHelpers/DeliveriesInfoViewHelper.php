<?php

namespace PIPEU\Accounting\ViewHelpers;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class DeliveriesInfoViewHelper
 *
 * @package PIPEU\Accounting\ViewHelpers
 */
class DeliveriesInfoViewHelper extends AbstractViewHelper {

	/**
	 * @param QueryResultInterface $deliveries
	 * @return string
	 */
	public function render(QueryResultInterface $deliveries) {

		$data['summary'] = 0;

		/** @var Invoice $document */
		while ($document = $deliveries->current()) {
			$data['summary'] += $document->getSummary()->getValue();
			$deliveries->next();
		}

		$this->templateVariableContainer->add('summary', new Money($data['summary']));
		$content = $this->renderChildren();
		$this->templateVariableContainer->remove('summary');

		return $content;
	}
}
