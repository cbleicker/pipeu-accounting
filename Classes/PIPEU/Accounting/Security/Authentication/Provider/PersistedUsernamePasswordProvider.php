<?php
namespace PIPEU\Accounting\Security\Authentication\Provider;

/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authentication\TokenInterface;
use TYPO3\Flow\Security\Authentication\Provider\PersistedUsernamePasswordProvider as FlowPersistedUsernamePasswordProvider;

/**
 * Class PersistedUsernamePasswordProvider
 *
 * @package PIPEU\Accounting\Security\Authentication\Provider
 */
class PersistedUsernamePasswordProvider extends FlowPersistedUsernamePasswordProvider {

	/**
	 * Authenticates against accounts with a Typo3BackendProvider provider
	 *
	 * @param TokenInterface $authenticationToken
	 * @return void
	 */
	public function authenticate(TokenInterface $authenticationToken) {
		$this->name = 'Typo3BackendProvider';
		parent::authenticate($authenticationToken);
	}
}
