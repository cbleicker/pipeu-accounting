<?php

namespace PIPEU\Accounting\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PIPEU\Accounting\Domain\Dto\TransactionFilter;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use PIPEU\Factura\Service\Converter\View\ViewFactory;
use PIPEU\Payment\Domain\Model\Abstracts\AbstractLog;
use PIPEU\Site\Service\Converter\DocumentToPdfConverter;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\I18n\Locale;
use TYPO3\Flow\I18n\Service as I18nService;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Mvc\Controller\ControllerContext;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Property\TypeConverter\DateTimeConverter;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Resource\Resource as FileResource;
use TYPO3\Flow\Resource\ResourceManager;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Utility\Environment;
use TYPO3\Flow\Utility\MediaTypes;

/**
 * Class TransactionController
 *
 * @package PIPEU\Accounting\Controller
 */
class TransactionController extends ActionController {

	const DOCUMENT_TEMPLATE_NAME = 'List', STYLE_TEMPLATE_NAME = 'Style', COLOR_TEMPLATE_NAME = 'Color';

	/**
	 * @var I18nService
	 * @Flow\Inject
	 */
	protected $i18nService;

	/**
	 * @var Environment
	 * @Flow\Inject
	 */
	protected $environment;

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @var ViewFactory
	 * @Flow\Inject
	 */
	protected $viewFactory;

	/**
	 * @var ResourceManager
	 * @Flow\Inject
	 */
	protected $resourceManager;

	/**
	 * @return void
	 */
	public function indexAction() {
		$filter = new TransactionFilter();
		$this->view->assign('filter', $filter);
	}

	/**
	 * @return void
	 */
	protected function initializeFilterAction() {
		if ($this->arguments->hasArgument('filter')) {
			$filterMapping = $this->arguments->getArgument('filter')->getPropertyMappingConfiguration();
			$filterMapping->allowProperties('startDate', 'endDate');
			$filterMapping->forProperty('startDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
			$filterMapping->forProperty('endDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
		}
	}

	/**
	 * @param TransactionFilter $filter
	 * @return void
	 */
	public function filterAction(TransactionFilter $filter = NULL) {
		$this->view->assign('logs', $this->getLogs($filter))->assign('filter', $filter);
	}

	/**
	 * @return void
	 */
	protected function initializeDownloadAction() {
		if ($this->arguments->hasArgument('filter')) {
			$filterMapping = $this->arguments->getArgument('filter')->getPropertyMappingConfiguration();
			$filterMapping->allowProperties('startDate', 'endDate');
			$filterMapping->forProperty('startDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
			$filterMapping->forProperty('endDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
		}
	}

	/**
	 * @param TransactionFilter $filter
	 * @return void
	 */
	public function downloadAction(TransactionFilter $filter = NULL) {

		$temporaryDirectory = $this->environment->getPathToTemporaryDirectory();
		$transactionList = $this->createTransactionDocument($filter);
		$transactionListPathAndFileName = $this->resourceManager->getPersistentResourcesStorageBaseUri() . (string)$transactionList;
		$packageFileName = $transactionList->getFilename() . '.tar';
		$packagePathAndFileName = $temporaryDirectory . $packageFileName;
		@unlink($packagePathAndFileName);

		$package = new \PharData($packagePathAndFileName);
		$package->addFile($transactionListPathAndFileName, $transactionList->getFilename());

		$logs = $this->getLogs($filter);

		$documentsMissing = FALSE;

		/** @var AbstractLog $log */
		while ($log = $logs->current()) {

			$mainDocument = $log->getDocument()->getMainDocument();
			$mainDocumentResource = $this->findDocumentResource($mainDocument);
			if (!($mainDocumentResource instanceof FileResource)) {
				$this->emitMissingDownload($mainDocument, $this->controllerContext);
				$documentsMissing = TRUE;
			} else {
				$resourcePathAndFileName = $this->resourceManager->getPersistentResourcesStorageBaseUri() . (string)$mainDocumentResource;
				$package->addFile($resourcePathAndFileName, $mainDocumentResource->getFilename());
			}

			$childDocuments = $mainDocument->getChildDocuments();
			while ($childDocument = $childDocuments->current()) {
				$childDocumentResource = $this->findDocumentResource($childDocument);
				if (!($childDocumentResource instanceof FileResource)) {
					$this->emitMissingDownload($childDocument, $this->controllerContext);
					$documentsMissing = TRUE;
				} else {
					$resourcePathAndFileName = $this->resourceManager->getPersistentResourcesStorageBaseUri() . (string)$childDocumentResource;
					$package->addFile($resourcePathAndFileName, $childDocumentResource->getFilename());
				}
				$childDocuments->next();
			}
			$logs->next();
		}

		if ($documentsMissing === TRUE) {
			@unlink($packagePathAndFileName);
			$this->addFlashMessage('There where not all Documents available as "' . DocumentToPdfConverter::TARGET_TYPE . '". Starting autogeneration now. Please try again later.', 'Queued', Message::SEVERITY_NOTICE, array(), 1433270843);
			$this->forward('filter', NULL, NULL, $this->request->getHttpRequest()->getArguments());
		}

		$this->response->setHeader('Content-Type', MediaTypes::getMediaTypeFromFilename($packageFileName));
		$this->response->setHeader('Content-Disposition', 'attachment; filename="' . $packageFileName . '"');
		$this->response->setContent(file_get_contents($packagePathAndFileName));
		$this->response->send();
	}

	/**
	 * @param TransactionFilter $filter
	 * @return Collection<AbstractLog>
	 */
	protected function getLogs(TransactionFilter $filter = NULL) {
		if ($filter === NULL) {
			$filter = new TransactionFilter();
		}

		$query = $this->persistenceManager->createQueryForType(AbstractLog::class)->setOrderings($filter->getOrderings());

		$constraint = [];

		if ($filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate()->setTime(0,0,0));
		}

		if ($filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate()->setTime(23,59,59));
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		$logs = new ArrayCollection($query->execute()->toArray());

		return $logs->filter(function (AbstractLog $log) use ($filter) {
			if ($filter->getTransactionType() === 'all') {
				return TRUE;
			}
			$logData = $log->getData();
			return $filter->getTransactionType() === Arrays::getValueByPath($logData, 'txaction');
		});
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitMissingDownload(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param TransactionFilter $filter
	 * @return string
	 */
	protected function generateFileName(TransactionFilter $filter) {
		$namePartitions = [];
		$namePartitions[] = $filter->getStartDate() === NULL ? '0.0.00' : $filter->getStartDate()->format('d.m.y');
		$namePartitions[] = $filter->getEndDate() === NULL ? date('d.m.y', time()) : $filter->getEndDate()->format('d.m.y');
		$namePartitions[] = $filter->getTransactionType();
		return implode('-', $namePartitions) . '.pdf';
	}

	/**
	 * @param InterfaceDocument $source
	 * @return string
	 */
	protected function generateDocumentFileName(InterfaceDocument $source) {
		return $source->getType() . '.' . $this->persistenceManager->getIdentifierByObject($source) . '.' . DocumentToPdfConverter::TARGET_TYPE;
	}

	/**
	 * @param InterfaceDocument $document
	 * @return FileResource
	 */
	protected function findDocumentResource(InterfaceDocument $document) {
		$query = $this->persistenceManager->createQueryForType(FileResource::class);
		$query->matching($query->equals('filename', $this->generateDocumentFileName($document)));
		return $query->execute()->getFirst();
	}

	/**
	 * @param InterfaceDocument $document
	 * @return FileResource
	 */
	protected function findResource(InterfaceDocument $document) {
		$query = $this->persistenceManager->createQueryForType(FileResource::class);
		$query->matching($query->equals('filename', $this->generateFileName($document)));
		return $query->execute()->getFirst();
	}

	/**
	 * @param TransactionFilter $filter
	 * @return FileResource
	 */
	public function createTransactionDocument(TransactionFilter $filter) {
		$logs = $this->getLogs($filter);

		$this->i18nService->getConfiguration()->setCurrentLocale(new Locale('de_DE'));

		$styleView = $this->viewFactory->create('PIPEU.Accounting:' . static::STYLE_TEMPLATE_NAME);
		$style = $styleView->render();

		$documentView = $this->viewFactory->create('PIPEU.Accounting:Transaction/' . static::DOCUMENT_TEMPLATE_NAME);
		$documentView->assign('logs', $logs)->assign('filter', $filter);
		$document = $documentView->render();

		$facade = \PHPPdf\Core\FacadeBuilder::create()->build();
		$content = $facade->render($document, $style);

		$resource = $this->resourceManager->createResourceFromContent($content, $this->generateFileName($filter));
		return $resource;
	}
}
