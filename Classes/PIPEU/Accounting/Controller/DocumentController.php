<?php
namespace PIPEU\Accounting\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "PIPEU.Accounting".      *
 *                                                                        *
 *                                                                        */

use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\Domain\Repository\Documents\DocumentRepository;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Mvc\Controller\ControllerContext;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Resource\Resource as FileResource;
use TYPO3\Flow\Utility\MediaTypes;
use TYPO3\Flow\Mvc\Controller\ActionController;

/**
 * Class DocumentController
 *
 * @package PIPEU\Accounting\Controller
 */
class DocumentController extends ActionController {

	/**
	 * @var array
	 */
	protected $documentSorting = array(
		'serialNumber' => QueryInterface::ORDER_DESCENDING
	);

	/**
	 * @var DocumentRepository
	 * @Flow\Inject
	 */
	protected $documentRepository;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->documentRepository->setDefaultOrderings($this->documentSorting);
		$this->view->assign('documents', $this->documentRepository->findAllUnarchived());
	}

	/**
	 * @return void
	 */
	public function archiveAction() {
		$this->documentRepository->setDefaultOrderings($this->documentSorting);
		$this->view->assign('documents', $this->documentRepository->findAllArchived());
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function downloadAction(AbstractDocument $document) {
		$resource = $this->findResource($document);
		if ($resource instanceof FileResource) {
			$this->response->setHeader('Content-type', MediaTypes::getMediaTypeFromFilename($resource->getFilename()));
			$this->response->setHeader('Content-disposition', 'attachment; filename="' . $document->getSerialNumber() . '.' . $resource->getFileExtension() . '"');
			$this->response->setContent(file_get_contents($resource->getUri()));
			$this->response->send();
		} else {
			$this->emitMissingDownload($document, $this->controllerContext);
			$this->addFlashMessage('Document is queued', 'Queued', Message::SEVERITY_NOTICE, array(), 1401819355);
			$this->redirect($this->request->getReferringRequest()->getControllerActionName());
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitMissingDownload(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param InterfaceDocument $document
	 * @return string
	 */
	protected function generateFileName(InterfaceDocument $document) {
		return $document->getType() . '.' . $this->persistenceManager->getIdentifierByObject($document) . '.pdf';
	}

	/**
	 * @param InterfaceDocument $document
	 * @return FileResource
	 */
	protected function findResource(InterfaceDocument $document) {
		$resourceType = 'TYPO3\Flow\Resource\Resource';
		$query = $this->persistenceManager->createQueryForType($resourceType);
		$query->matching(
			$query->equals('filename', $this->generateFileName($document))
		);
		return $query->execute()->getFirst();
	}
}