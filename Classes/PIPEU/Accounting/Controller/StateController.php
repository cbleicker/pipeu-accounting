<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Accounting\Controller;

use PIPEU\Factura\Domain\Abstracts\AbstractFacturaDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Factura\Domain\Model\State;
use PIPEU\PayOne\Controller\Abstracts\AbstractTransactionController;
use PIPEU\PayOne\Domain\Model\Log;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ControllerContext;

/**
 * Class StateController
 *
 * @package PIPEU\Accounting\Controller
 */
class StateController extends AbstractTransactionController {

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function appointedAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_APPOINTED, InterfaceState::SEVERITY_NOTICE, 'Appointed');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitAppointed($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitAppointed(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function captureAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_CAPTURE, InterfaceState::SEVERITY_NOTICE, 'Capture');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitCapture($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitCapture(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function paidAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_PAID, InterfaceState::SEVERITY_OK, 'Paid');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitPaid($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitPaid(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function underpaidAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_UNDERPAID, InterfaceState::SEVERITY_WARNING, 'Underpaid');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitUnderpaid($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitUnderpaid(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function cancelationAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_CANCELATION, InterfaceState::SEVERITY_NOTICE, 'Cancelation');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitCancelation($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitCancelation(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function refundAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_REFUNDED, InterfaceState::SEVERITY_OK, 'Refunded');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitRefund($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitRefund(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function debitAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_DEBIT, InterfaceState::SEVERITY_OK, 'Debit');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitDebit($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitDebit(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function reminderAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_REMINDED, InterfaceState::SEVERITY_NOTICE, 'Reminder');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitReminder($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitReminder(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function vauthorizationAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_VAUTHORIZATION, InterfaceState::SEVERITY_NOTICE, 'Virtual Authorization');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitVauthorization($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitVauthorization(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function vsettlementAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_VSETTLEMENT, InterfaceState::SEVERITY_NOTICE, 'Virtual Settlement');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitVsettlement($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitVsettlement(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function transferAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_TRANSFER, InterfaceState::SEVERITY_NOTICE, 'Transfer');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitTransfer($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitTransfer(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractFacturaDocument $param
	 * @return void
	 */
	public function invoiceAction(AbstractFacturaDocument $param) {
		try {
			$log = new Log($this->request->getHttpRequest()->getArguments(), $param, $this->request->getHttpRequest()->hasArgument('txid') ? $this->request->getHttpRequest()->getArgument('txid') : NULL);
			$this->persistenceManager->add($log);
			$this->persistenceManager->whitelistObject($log);

			$state = new State(InterfaceState::CODE_INVOICE, InterfaceState::SEVERITY_NOTICE, 'Invoice');
			$param->setPrimaryState($state);
			$this->persistenceManager->update($param);
			$this->persistenceManager->whitelistObject($state);
			$this->persistenceManager->whitelistObject($param);
			$this->persistenceManager->persistAll(TRUE);
			$this->emitInvoice($param, $this->controllerContext);
		} catch (\Exception $exception) {
			$this->transactionLogger->logException($exception);
		} finally {
			$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
		}
	}

	/**
	 * @param AbstractFacturaDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitInvoice(AbstractFacturaDocument $document, ControllerContext $controllerContext) {
	}
}
