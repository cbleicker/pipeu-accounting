<?php
namespace PIPEU\Accounting\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "PIPEU.Accounting".      *
 *                                                                        *
 *                                                                        */

use PIPEU\Accounting\Domain\Dto\Filter;
use PIPEU\Factura\Domain\Model\Documents\Credit;
use PIPEU\Factura\Domain\Model\Documents\Delivery;
use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Documents\Order;
use PIPEU\Factura\Domain\Model\Documents\Reverse;
use PIPEU\Factura\Domain\Repository\Documents\DocumentRepository;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Flow\Property\TypeConverter\DateTimeConverter;

/**
 * Class InfoController
 *
 * @package PIPEU\Accounting\Controller
 */
class InfoController extends ActionController {

	/**
	 * @var array
	 */
	protected $documentSorting = array(
		'serialNumber' => QueryInterface::ORDER_DESCENDING
	);

	/**
	 * @var DocumentRepository
	 * @Flow\Inject
	 */
	protected $documentRepository;

	/**
	 * @return void
	 */
	protected function initializeIndexAction() {
		if ($this->arguments->hasArgument('filter')) {
			$filterMapping = $this->arguments->getArgument('filter')->getPropertyMappingConfiguration();
			$filterMapping->allowProperties('startDate', 'endDate');
			$filterMapping->forProperty('startDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
			$filterMapping->forProperty('endDate')->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
		}
	}

	/**
	 * @param Filter $filter
	 * @return void
	 */
	public function indexAction(Filter $filter = NULL) {
		$data = [];
		$data['invoices'] = $this->getInvoices($filter);
		$data['credits'] = $this->getCredits($filter);
		$data['reverses'] = $this->getReverses($filter);
		$data['orders'] = $this->getOrders($filter);
		$data['deliveries'] = $this->getDeliveries($filter);

		$this->view->assignMultiple($data);
		$this->view->assign('filter', $filter);
	}

	/**
	 * @param Filter $filter
	 * @return QueryResultInterface
	 */
	private function getInvoices(Filter $filter = NULL) {
		$query = $this->persistenceManager->createQueryForType(Invoice::class);
		$constraint = [];

		if ($filter !== NULL && $filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate());
		}

		if ($filter !== NULL && $filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate());
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		return $query->execute();
	}

	/**
	 * @param Filter $filter
	 * @return QueryResultInterface
	 */
	private function getCredits(Filter $filter = NULL) {
		$query = $this->persistenceManager->createQueryForType(Credit::class);
		$constraint = [];

		if ($filter !== NULL && $filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate());
		}

		if ($filter !== NULL && $filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate());
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		return $query->execute();
	}

	/**
	 * @param Filter $filter
	 * @return QueryResultInterface
	 */
	private function getReverses(Filter $filter = NULL) {
		$query = $this->persistenceManager->createQueryForType(Reverse::class);
		$constraint = [];

		if ($filter !== NULL && $filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate());
		}

		if ($filter !== NULL && $filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate());
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		return $query->execute();
	}

	/**
	 * @param Filter $filter
	 * @return QueryResultInterface
	 */
	private function getOrders(Filter $filter = NULL) {
		$query = $this->persistenceManager->createQueryForType(Order::class);
		$constraint = [];

		if ($filter !== NULL && $filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate());
		}

		if ($filter !== NULL && $filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate());
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		return $query->execute();
	}

	/**
	 * @param Filter $filter
	 * @return QueryResultInterface
	 */
	private function getDeliveries(Filter $filter = NULL) {
		$query = $this->persistenceManager->createQueryForType(Delivery::class);
		$constraint = [];

		if ($filter !== NULL && $filter->getStartDate() !== NULL) {
			$constraint[] = $query->greaterThanOrEqual('dateTime', $filter->getStartDate());
		}

		if ($filter !== NULL && $filter->getEndDate() !== NULL) {
			$constraint[] = $query->lessThanOrEqual('dateTime', $filter->getEndDate());
		}

		if (count($constraint) > 0) {
			$query->matching(
				$query->logicalAnd(
					$constraint
				)
			);
		}

		return $query->execute();
	}
}