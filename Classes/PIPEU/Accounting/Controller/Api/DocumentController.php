<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Accounting\Controller\Api;

use PIPEU\Accounting\Mvc\Controller\AbstractRestController;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use PIPEU\Factura\Domain\Abstracts\AbstractState;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Factura\Domain\Model\State;
use TYPO3\Flow\Mvc\Controller\ControllerContext;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\Domain\Model\Documents\Credit;
use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Domain\Model\Documents\Reverse;
use PIPEU\Factura\Domain\Model\Documents\Delivery;
use TYPO3\Flow\Property\PropertyMapper;
use PIPEU\Factura\Domain\Repository\Documents\DocumentRepository;

/**
 * Class DocumentController
 *
 * @package PIPEU\Accounting\Controller\Api
 */
class DocumentController extends AbstractRestController {

	/**
	 * Name of the action method argument which acts as the resource for the
	 * RESTful controller. If an argument with the specified name is passed
	 * to the controller, the show, update and delete actions can be triggered
	 * automatically.
	 *
	 * @var string
	 */
	protected $resourceArgumentName = 'document';

	/**
	 * @var DocumentRepository
	 * @Flow\Inject
	 */
	protected $documentRepository;

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * @return void
	 */
	protected function initializeViewReversedAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function reversedAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_REVERSED, InterfaceState::SEVERITY_ERROR, 'Reversed');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitReversed($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitReversed(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewRefundedAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function refundedAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_REFUNDED, InterfaceState::SEVERITY_OK, 'Refunded');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitRefunded($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitRefunded(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewPaidAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function paidAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_PAID, InterfaceState::SEVERITY_OK, 'Paid');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitPaid($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitPaid(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewShippedAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function shippedAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_SHIPPED, InterfaceState::SEVERITY_OK, 'Shipped');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitShipped($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitShipped(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewApprovementReminderAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function approvementReminderAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_REMINDED, InterfaceState::SEVERITY_NOTICE, 'Reminded');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitApprovementReminder($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitApprovementReminder(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewPaymentReminderAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function paymentReminderAction(AbstractDocument $document) {
		$state = new State(InterfaceState::CODE_REMINDED, InterfaceState::SEVERITY_NOTICE, 'Reminded');
		$document->setPrimaryState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitPaymentReminder($persistedDocument, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitPaymentReminder(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewRemoveStateAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'primaryState'),
			'primaryState' => array(
				'_exposeObjectIdentifier' => TRUE
			)
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @param AbstractState $state
	 * @return void
	 */
	public function removeStateAction(AbstractDocument $document, AbstractState $state) {
		$document->removeState($state);
		$this->persistenceManager->update($document);
		$this->persistenceManager->whitelistObject($document);
		$this->persistenceManager->whitelistObject($state);
		$this->persistenceManager->persistAll(TRUE);
		$persistedDocument = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($document));
		$this->view->assign($this->resourceArgumentName, $persistedDocument);
		$this->emitRemoveState($persistedDocument, $state, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param AbstractState $state
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitRemoveState(AbstractDocument $document, AbstractState $state, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewReverseAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'dateTime', 'serialNumber', 'type', 'primaryPersonName', 'secondaryPersonName', 'primaryPostal', 'secondaryPostal', 'summaryGross')
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function reverseAction(AbstractDocument $document) {
		$targetType = 'PIPEU\Factura\Domain\Model\Documents\Reverse';
		/** @var Credit $result */
		$result = $this->propertyMapper->convert($document->getMainDocument(), $targetType);
		$this->persistenceManager->add($result);
		$this->persistenceManager->persistAll();
		$this->persistenceManager->clearState();
		$persistedResult = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($result));
		$this->view->assign($this->resourceArgumentName, $persistedResult);
		$this->emitReverseCreated($result, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitReverseCreated(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewDeliveryAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'dateTime', 'serialNumber', 'type', 'primaryPersonName', 'secondaryPersonName', 'primaryPostal', 'secondaryPostal', 'summaryGross')
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function deliveryAction(AbstractDocument $document) {
		$targetType = 'PIPEU\Factura\Domain\Model\Documents\Delivery';
		/** @var Credit $result */
		$result = $this->propertyMapper->convert($document->getMainDocument(), $targetType);
		$this->persistenceManager->add($result);
		$this->persistenceManager->persistAll();
		$this->persistenceManager->clearState();
		$persistedResult = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($result));
		$this->view->assign($this->resourceArgumentName, $persistedResult);
		$this->emitDeliveryCreated($result, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitDeliveryCreated(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	protected function initializeViewCreditAction() {
		$viewConfiguration = array(
			'_only' => array('identity', 'dateTime', 'serialNumber', 'type', 'primaryPersonName', 'secondaryPersonName', 'primaryPostal', 'secondaryPostal', 'summaryGross')
		);
		$configuration = array($this->resourceArgumentName => $viewConfiguration);
		$this->view->setConfiguration($configuration);
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function creditAction(AbstractDocument $document) {
		$targetType = 'PIPEU\Factura\Domain\Model\Documents\Credit';
		/** @var Credit $result */
		$result = $this->propertyMapper->convert($document->getMainDocument(), $targetType);
		$this->persistenceManager->add($result);
		$this->persistenceManager->persistAll();
		$this->persistenceManager->clearState();
		$persistedResult = $this->documentRepository->findByIdentifier($this->persistenceManager->getIdentifierByObject($result));
		$this->view->assign($this->resourceArgumentName, $persistedResult);
		$this->emitCreditCreated($result, $this->controllerContext);
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitCreditCreated(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function archivedAction(AbstractDocument $document) {
		$this->view->assign($this->resourceArgumentName, $this->archiveDocument($document));
	}

	/**
	 * @param AbstractDocument $document
	 * @return \Doctrine\Common\Collections\Collection
	 */
	protected function archiveDocument(AbstractDocument $document){
		$documentsToArchive = clone $document->getMainDocument()->getChildDocuments();
		$documentsToArchive->add($document->getMainDocument());
		/** @var AbstractDocument $documentToArchive */
		while($documentToArchive = $documentsToArchive->current()){
			$documentToArchive->setArchived();
			$this->persistenceManager->update($documentToArchive);
			$this->persistenceManager->whitelistObject($documentToArchive);
			$documentsToArchive->next();
		}
		$this->persistenceManager->persistAll(TRUE);
		return $documentsToArchive;
	}


}
