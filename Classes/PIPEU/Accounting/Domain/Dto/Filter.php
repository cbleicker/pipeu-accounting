<?php

namespace PIPEU\Accounting\Domain\Dto;

use DateTime;

/**
 * Class Filter
 *
 * @package PIPEU\Accounting\Domain\Dto
 */
class Filter {

	/**
	 * @var DateTime
	 */
	protected $startDate;

	/**
	 * @var DateTime
	 */
	protected $endDate;

	/**
	 * @param DateTime $endDate
	 * @param DateTime $startDate
	 */
	public function __construct(DateTime $endDate = NULL, DateTime $startDate = NULL) {
		$this->endDate = $endDate;
		$this->startDate = $startDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getStartDate() {
		return $this->startDate;
	}
}
