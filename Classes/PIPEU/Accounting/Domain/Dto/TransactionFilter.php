<?php

namespace PIPEU\Accounting\Domain\Dto;

use DateTime;
use TYPO3\Flow\Persistence\QueryInterface;

/**
 * Class TransactionFilter
 *
 * @package PIPEU\Accounting\Domain\Dto
 */
class TransactionFilter {

	/**
	 * @var array
	 */
	protected $orderings = [
		'dateTime' => QueryInterface::ORDER_DESCENDING
	];

	/**
	 * @var string
	 */
	protected $transactionType = 'paid';

	/**
	 * @var DateTime
	 */
	protected $startDate;

	/**
	 * @var DateTime
	 */
	protected $endDate;

	/**
	 * @param DateTime $endDate
	 * @param DateTime $startDate
	 * @param string $transactionType
	 */
	public function __construct(DateTime $endDate = NULL, DateTime $startDate = NULL, $transactionType = 'paid') {
		$this->endDate = $endDate;
		$this->startDate = $startDate;
		$this->transactionType = $transactionType;
	}

	/**
	 * @return \DateTime
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * @return array
	 */
	public function getOrderings() {
		return $this->orderings;
	}

	/**
	 * @return string
	 */
	public function getTransactionType() {
		return $this->transactionType;
	}
}
